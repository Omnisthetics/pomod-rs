use std::{
    fmt::Display,
    fs::{create_dir, read_to_string, remove_dir_all, remove_file, File},
    io::{Read, Write},
    os::unix::net::{UnixListener, UnixStream},
    path::Path,
    process::Command,
    sync::{Arc, Mutex},
    thread::{self, sleep},
    time::{Duration, Instant},
};

use anyhow::{anyhow, Result};
use clap::{Parser, Subcommand};
use daemonize::Daemonize;
use notify_rust::Notification;

// Temp file paths
const TMP_DIR: &str = "/tmp/pomod-rs";
const OUT_FILE: &str = "/tmp/pomod-rs/pomod-rs.out";
const ERR_FILE: &str = "/tmp/pomod-rs/pomod-rs.err";
const PID_FILE: &str = "/tmp/pomod-rs/pomod-rs.pid";
const INFO_SOCKET: &str = "/tmp/pomod-rs/pomod-rs_info.sock";
const COMMAND_SOCKET: &str = "/tmp/pomod-rs/pomod-rs_command.sock";

/// Daemon commands
#[derive(Subcommand, Debug)]
enum Commands {
    /// Initialises the daemon
    Init,
    /// Pauses/unpauses the timer
    Toggle,
    /// Resets the current pomodoro/break timer
    Reset,
    /// Ends the current pomodoro/break
    Skip,
    /// Displays the remaining time and pomodoro/break number
    Info,
    /// Kills the daemon
    Kill,
}

impl From<u8> for Commands {
    fn from(value: u8) -> Self {
        match value {
            0 => Commands::Init,
            1 => Commands::Toggle,
            2 => Commands::Reset,
            3 => Commands::Skip,
            4 => Commands::Info,
            _ => Commands::Kill,
        }
    }
}

impl From<Commands> for u8 {
    fn from(value: Commands) -> Self {
        value as u8
    }
}

/// Command-line arguments
#[derive(Parser, Debug)]
#[command(version, about)]
struct Args {
    /// Daemon command
    #[command(subcommand)]
    command: Commands,
    /// Pomodoro count before a long break
    #[arg(short = 'c', long = "count", default_value_t = 4)]
    pomo_count: u32,
    /// Pomodoro length in mins
    #[arg(short = 'p', long = "pomodoro", default_value_t = 25)]
    pomo_mins: u32,
    /// Short break length in mins
    #[arg(short = 's', long = "short", default_value_t = 5)]
    short_break_mins: u32,
    /// Long break length in mins
    #[arg(short = 'l', long = "long", default_value_t = 20)]
    long_break_mins: u32,
    /// Info command output format
    #[arg(short = 'f', long = "format", default_value_t = String::from("$t $i$p/$c"))]
    info_format: String,
}

/// Interval type
#[derive(PartialEq, Eq)]
enum Interval {
    /// A work interval
    Pomodoro,
    /// A short break between pomodoros
    ShortBreak,
    /// A long break after all pomodoros
    LongBreak,
}

impl Display for Interval {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let str = match &self {
            Interval::Pomodoro => "pomodoro",
            Interval::ShortBreak => "shortbreak",
            Interval::LongBreak => "longbreak",
        };

        write!(f, "{}", str)
    }
}

/// Daemon state
struct State {
    /// The current interval type
    current_interval: Interval,
    /// The current pomodoro number
    current_pomo: u32,
    /// Remaining time for current interval in secs
    remaining_time: u32, // TODO: Perhaps change into a duration
    /// Whether the interval timer is running or not
    active: bool,
    /// Kills the daemon when true
    kill: bool,
    /// Pomodoro count before a long break
    pomo_count: u32,
    /// Pomodoro length in mins
    pomo_mins: u32,
    /// Short break length in mins
    short_break_mins: u32,
    /// Long break length in mins
    long_break_mins: u32,
    /// Info command output format
    info_format: String,
}

impl From<Args> for State {
    fn from(value: Args) -> Self {
        State {
            current_interval: Interval::Pomodoro,
            current_pomo: 1,
            remaining_time: value.pomo_mins.saturating_mul(60),
            active: false,
            kill: false,
            pomo_count: value.pomo_count,
            pomo_mins: value.pomo_mins,
            short_break_mins: value.short_break_mins,
            long_break_mins: value.long_break_mins,
            info_format: value.info_format,
        }
    }
}

impl State {
    /// Ends the current interval
    fn end_interval(&mut self) {
        self.active = false;

        // Set next interval type and increment pomodoro count
        if self.current_interval == Interval::Pomodoro {
            self.current_interval = if self.current_pomo < self.pomo_count {
                Interval::ShortBreak
            } else {
                Interval::LongBreak
            };
        } else {
            self.current_interval = Interval::Pomodoro;
            self.current_pomo = if self.current_pomo < self.pomo_count {
                self.current_pomo + 1
            } else {
                1
            };
        }

        // Set timer for next interval
        self.remaining_time = match self.current_interval {
            Interval::Pomodoro => self.pomo_mins * 60,
            Interval::ShortBreak => self.short_break_mins * 60,
            Interval::LongBreak => self.long_break_mins * 60,
        };
    }

    /// Modifies the daemon state based on the provided command
    fn handle_command(&mut self, command: Commands) {
        match command {
            Commands::Init => (),
            Commands::Toggle => {
                self.active = !self.active;
            }
            Commands::Reset => {
                self.remaining_time = match self.current_interval {
                    Interval::Pomodoro => self.pomo_mins * 60,
                    Interval::ShortBreak => self.short_break_mins * 60,
                    Interval::LongBreak => self.long_break_mins * 60,
                };
            }
            Commands::Skip => {
                self.end_interval();
            }
            Commands::Info => {
                let time = {
                    let mins = self.remaining_time / 60;
                    let secs = self.remaining_time % 60;

                    format!("{}:{:02}", mins, secs)
                };

                let active = if self.active { "active" } else { "paused" };

                // Format info string
                let info = self
                    .info_format
                    .replace("$a", active)
                    .replace("$t", time.as_str())
                    .replace("$i", &self.current_interval.to_string())
                    .replace("$p", &self.current_pomo.to_string())
                    .replace("$c", &self.pomo_count.to_string());

                // Send info string via Unix socket
                if let Ok(mut stream) = UnixStream::connect(INFO_SOCKET) {
                    let _ = stream.write(info.as_bytes());
                }
            }
            Commands::Kill => {
                self.kill = true;
            }
        }
    }
}

/// Cleans the temp directory and starts daemon
fn init(args: Args) -> Result<()> {
    if is_daemon_running()? {
        return Err(anyhow!("Process is already running!"));
    }

    cleanup()?;
    create_dir(TMP_DIR)?;
    daemonise(args)?;
    cleanup()?;

    Ok(())
}

/// Requests info string from the daemon
fn info() -> Result<()> {
    if !is_daemon_running()? {
        return Err(anyhow!("Process is not running!"));
    }

    let listener = UnixListener::bind(INFO_SOCKET)?;

    send_daemon_command(Commands::Info)?;

    // Wait for and display response from daemon
    // WARNING: Process will hang if daemon is killed before a response is received
    if let Ok((mut stream, _)) = listener.accept() {
        let mut info = String::new();
        stream.read_to_string(&mut info)?;
        println!("{}", info);
    }

    remove_file(INFO_SOCKET)?;

    Ok(())
}

/// Starts daemonised process
fn daemonise(args: Args) -> Result<()> {
    let stdout = File::create(OUT_FILE)?;
    let stderr = File::create(ERR_FILE)?;

    let daemon = Daemonize::new()
        .pid_file(PID_FILE)
        .stdout(stdout)
        .stderr(stderr);

    if let Err(error) = daemon.start() {
        cleanup()?;
        return Err(error.into());
    }

    let state: State = args.into();
    let state_mutex = Arc::new(Mutex::new(state));
    let listener = UnixListener::bind(COMMAND_SOCKET)?;

    // Run loops on separate threads
    thread::scope(|s| {
        s.spawn(|| timer_loop(&state_mutex));
        s.spawn(|| command_loop(&state_mutex, &listener));
    });

    Ok(())
}

/// Listens for and handles commands sent from non-daemon processes via Unix socket
fn command_loop(state_mutex: &Arc<Mutex<State>>, listener: &UnixListener) {
    for stream in listener.incoming().flatten() {
        for byte in stream.bytes().flatten() {
            match state_mutex.lock() {
                Ok(mut state_guard) => {
                    state_guard.handle_command(byte.into());

                    if state_guard.kill {
                        return;
                    }
                }
                Err(error) => eprintln!("MUTEX ERROR: {}", error),
            }
        }
    }
}

/// Updates timer max once a second
fn timer_loop(state_mutex: &Arc<Mutex<State>>) {
    loop {
        let start_time = Instant::now();
        let state_lock_result = state_mutex.lock();

        if let Ok(mut state_guard) = state_lock_result {
            if state_guard.active {
                state_guard.remaining_time = state_guard.remaining_time.saturating_sub(1);

                // End interval
                if state_guard.remaining_time == 0 {
                    state_guard.end_interval();

                    // Send notification
                    let body = match state_guard.current_interval {
                        Interval::Pomodoro => "Break time is up!",
                        _ => "Pomodoro time is up!",
                    };

                    let notif_result = Notification::new()
                        .summary("Pomodoro")
                        .body(body)
                        .urgency(notify_rust::Urgency::Critical)
                        .show();

                    if let Err(error) = notif_result {
                        eprintln!("NOTIF ERROR: {}", error);
                    }
                }
            }

            // Break endless loop if kill flag is set
            if state_guard.kill {
                return;
            }
        } else if let Err(error) = state_lock_result {
            eprintln!("MUTEX ERROR: {}", error);
        }

        // Sleep for a second max
        let loop_dur = Instant::now() - start_time;
        let sleep_dur = Duration::from_secs(1).saturating_sub(loop_dur);

        sleep(sleep_dur);
    }
}

/// Send command to daemon via Unix socket
fn send_daemon_command(command: Commands) -> Result<()> {
    if !is_daemon_running()? {
        return Err(anyhow!("Process is not running!"));
    }

    let mut stream = UnixStream::connect(COMMAND_SOCKET)?;

    stream.write_all(&[command.into()])?;

    Ok(())
}

/// Checks whether the daemon is running
fn is_daemon_running() -> Result<bool> {
    let pid_file_exists = Path::new(PID_FILE).try_exists()?;

    if !pid_file_exists {
        return Ok(false);
    }

    let pid = read_to_string(PID_FILE)?.trim().parse::<i32>()?;

    Command::new("kill")
        .args(["-0", &pid.to_string()])
        .output()
        .map(|o| o.stdout.is_empty() && o.stderr.is_empty())
        .map_err(|e| e.into())
}

/// Removes pomod-rs' temp directory
fn cleanup() -> Result<()> {
    if Path::new(TMP_DIR).is_dir() {
        remove_dir_all(TMP_DIR)?;
    }

    Ok(())
}

fn main() {
    let args = Args::parse();

    let command_result = match args.command {
        Commands::Init => init(args),
        Commands::Info => info(),
        command => send_daemon_command(command),
    };

    if let Err(err) = command_result {
        eprintln!("{}", err);
    }
}
